package scope;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


class Difference {
    private int[] elements;
    int maximumDifference;

    // Add your code here
    Difference(int [] a){
        this.elements = a;
    }

    void computeDifference(){
        //Se trata de encontrar la mayor diferencia entre 2 numeros del array
        int i = 0;
        int j = 1;
        while(i < this.elements.length){
            while(j < this.elements.length){
                int difference = Math.abs(this.elements[i]-this.elements[j]);
                if(this.maximumDifference < difference){
                    this.maximumDifference = difference;
                }
                j++;
            }
            i++;
            j = i + 1;
        }
    }

} // End of Difference class

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        sc.close();

        Difference difference = new Difference(a);

        difference.computeDifference();

        System.out.print(difference.maximumDifference);
    }
}