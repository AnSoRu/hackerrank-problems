package inheritance;

public class Student extends Person{

    private int[] testScores;

    /*
     *   Class Constructor
     *
     *   @param firstName - A string denoting the Person's first name.
     *   @param lastName - A string denoting the Person's last name.
     *   @param id - An integer denoting the Person's ID number.
     *   @param scores - An array of integers denoting the Person's test scores.
     */
    // Write your constructor here
        public Student(String firstName, String lastName, int identification,int [] scores){
            super(firstName,lastName,identification);
            this.testScores = scores;
        }
    /*
     *   Method Name: calculate
     *   @return A character denoting the grade.
     */
    // Write your method here
    public char calculate(){
        char result;
        double average;
        int totalSum = 0;
        for(int i = 0; i < testScores.length; i++){
            totalSum = totalSum + testScores[i];
        }
        average = totalSum/testScores.length;
        if(average < 40){
            result = 'T';
        }else if(average>=40 && average < 55){
            result = 'D';
        }else if(average>=55 && average < 70){
            result = 'P';
        }else if(average>=70 && average < 80){
            result = 'A';
        }else if(average>=80 && average < 90){
            result = 'E';
        }else{
            result = 'O';
        }
        return result;
    }
}