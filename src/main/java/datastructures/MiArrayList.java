package datastructures;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MiArrayList {


    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(scanner.nextLine()); //Numero de listas
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        List<ArrayList<Integer>> listaLista = new ArrayList<ArrayList<Integer>>();
        for(int i = 0; i < n; i++){
            String[] numbers = scanner.nextLine().split(" ");
            List<Integer> aux = new ArrayList<Integer>();
            for(int j = 0; j < numbers.length; j++){
                aux.add(Integer.valueOf(numbers[j]));
            }
            listaLista.add((ArrayList<Integer>) aux);
        }
        //Number of queries
        int q = Integer.parseInt(scanner.nextLine());
        for(int i = 0; i < q; i++){
            int x = scanner.nextInt(); //Lista x
            int y = scanner.nextInt(); //Elemento y
            if( x < listaLista.size()) {
                if (listaLista.get(x - 1) != null) {
                    if( y < listaLista.get(x - 1).size()){
                        if (listaLista.get(x - 1).get(y) != null) {
                            System.out.println(listaLista.get(x - 1).get(y));
                        } else {
                            System.out.println("ERROR!");
                        }
                    }else{
                        System.out.println("ERROR!");
                    }
                }
            }else{
                System.out.println("ERROR!");
            }
        }
        scanner.close();
    }
}
