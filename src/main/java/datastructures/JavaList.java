package datastructures;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JavaList {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        int n = Integer.parseInt(scanner.nextLine()); //Numero inicial de elementos en la lista
        String[] numbers = scanner.nextLine().split(" "); //Lista inicial
        List<Integer> listIntegers = new ArrayList<>();
        for(int i = 0; i < numbers.length; i++){
            listIntegers.add(Integer.valueOf(numbers[i]));
        }
        int q = Integer.parseInt(scanner.nextLine());
        for(int i = 0; i < q; i++){
            String accion = scanner.nextLine();
            if(accion.equals("Insert")){
                String [] numbers2 = scanner.nextLine().split(" ");
                int x = Integer.valueOf(numbers2[0]);
                int y = Integer.valueOf(numbers2[1]);
                listIntegers.add(x,y);
            }else if(accion.equals("Delete")){
                String [] numbers2 = scanner.nextLine().split(" ");
                int x = Integer.valueOf(numbers2[0]);
                listIntegers.remove(x);
            }else{
                System.out.println("No Action");
            }
        }
        StringBuilder sb = new StringBuilder();
        listIntegers.stream().forEach(e -> {sb.append(e);sb.append(" ");});
        System.out.println(sb);
    }
}
