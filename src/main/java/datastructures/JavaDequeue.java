package datastructures;

import java.util.*;

public class JavaDequeue {

    //Double-ended queue (pronunciado deck)
    //Los elementos pueden ser añadidos o eliminados desde el principio o el final (head) or (tail)
    //Se pueden implementar con LinkedList o ArrayDeque
    //Deque deque = new LinkedList<>();
    //Deque deque = new ArrayDeque<>();

    private static Scanner scanner = new Scanner(System.in);

    public static void noDequeSolution(){
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        Integer [] numbers = new Integer[n]; //Para que funcione el Arrays.asList tiene que ser Integer y no int
        for (int i = 0; i < n; i++) {
            int num = scanner.nextInt();
            numbers[i] = num;
        }
        int i = 0;
        int maximumUniqueNumbers = 0;
        while((i + m) <= numbers.length){
            Integer [] subarray = Arrays.copyOfRange(numbers,i,i+m);
            List<Integer> subarrayList = Arrays.asList(subarray);
            List<Integer> uniques = new ArrayList<>(new HashSet<>(subarrayList));
            if(uniques.size() > maximumUniqueNumbers){
                maximumUniqueNumbers = uniques.size();
            }
            i++;
        }
        System.out.println(maximumUniqueNumbers);
    }

    public void slowDequeSolution(){
        Scanner in = new Scanner(System.in);
        Deque deque = new ArrayDeque<>();
        int n = in.nextInt();
        int m = in.nextInt();
        Integer [] numbers = new Integer[n];
        for (int i = 0; i < n; i++) {
            int num = in.nextInt();
            numbers[i] = num;
        }
        deque.addAll(Arrays.asList(numbers));
        int maximumUniqueNumbers = 0;
        int i = 0;
        boolean recorridoCompletado = false;
        while(i <= deque.size() && !recorridoCompletado){
            if(deque.size()>=m){
                int j = 0;
                Object [] array = deque.toArray();
                Integer [] integerArray = new Integer[array.length];
                for(int k = 0; k < array.length; k++){
                    integerArray[k] = (Integer) array[k];
                }
                Integer [] subarray = Arrays.copyOfRange(integerArray,j,j+m);
                List<Integer> subarrayList = Arrays.asList(subarray);
                List<Integer> uniques = new ArrayList<>(new HashSet<>(subarrayList));
                if(uniques.size() > maximumUniqueNumbers){
                    maximumUniqueNumbers = uniques.size();
                }
                deque.removeFirst();
            }
            i++;
        }
        System.out.println(maximumUniqueNumbers);
    }

    public static void main(String[] args) {
        //noDequeSolution();
        Scanner in = new Scanner(System.in);
        Deque<Integer> deque = new ArrayDeque<>();
        HashSet<Integer> set = new HashSet<>();

        int n = in.nextInt();
        int m = in.nextInt();
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            int input = in.nextInt();

            deque.add(input);
            set.add(input);

            if (deque.size() == m) {
                if (set.size() > max) max = set.size();
                int first = deque.remove();
                if (!deque.contains(first)) set.remove(first);
            }
        }

        System.out.println(max);
    }
}
