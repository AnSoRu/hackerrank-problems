package datastructures;

import java.util.*;

public class BitSet {

    //BitSet implementa un vector de bits que crece a medida

    //Dados 2 BitSet de tamaño N inicializados a 0
    //Realizar series de M operaciones
    //Despues de cada operacion imprimir el resultado de ambos bitsets
    //Cada uno en una linea con los bits separados por espacios

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int bitSetSize = sc.nextInt();
        int numOpe = sc.nextInt();
        java.util.BitSet b1 = new java.util.BitSet();
        java.util.BitSet b2 = new java.util.BitSet();
        for(int i = 0; i < numOpe; i++){
            String operation = sc.next();
            int x = sc.nextInt();
            int y = sc.nextInt();
            if(operation.equals("AND")){
                if(x==1) b1.and(b2);
                else b2.and(b1);
            }else if(operation.equals("OR")){
                if(x==1) b1.or(b2);
                else b2.or(b1);
            }else if(operation.equals("XOR")){
                if(x==1) b1.xor(b2);
                else b2.xor(b1);
            }else if(operation.equals("FLIP")){
                if(x==1) b1.flip(y);
                else b2.flip(y);
            }else if(operation.equals("SET")){
                if(x==1) b1.set(y);
                else b2.set(y);
            }
            System.out.println(b1.cardinality()+" "+b2.cardinality());
        }
    }
}
