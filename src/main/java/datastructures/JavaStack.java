package datastructures;


import java.util.Scanner;
import java.util.Stack;

public class JavaStack {
    //Comprueba si los parentesis estan bien balanceados

    public static void main(String []argh)
    {
        Scanner sc = new Scanner(System.in);
        Stack<Character> stack = new Stack<>();
        while (sc.hasNext()) {
            String input=sc.next();
            char [] inputChars = input.toCharArray();
            boolean isCorrectString = false;
            for(int i = 0; i < inputChars.length;i++){
                if(inputChars[i] == '{' || inputChars[i] == '(' || inputChars[i] == '['){
                    stack.push(inputChars[i]);
                }else{
                    if(stack.empty()){
                        isCorrectString = false;
                        break;
                    }else{
                        if(inputChars[i] == '}'){
                            if(stack.peek()!='{'){
                                isCorrectString = false;
                                break;
                            }else{
                                stack.pop();
                                isCorrectString = true;
                            }
                        }else if(inputChars[i] == ')'){
                            if(stack.peek()!='('){
                                isCorrectString = false;
                                break;
                            }else{
                                stack.pop();
                                isCorrectString = true;
                            }
                        }else if(inputChars[i] == ']'){
                            if(stack.peek()!='['){
                                isCorrectString = false;
                                break;
                            }else{
                                stack.pop();
                                isCorrectString = true;
                            }
                        }
                    }
                }
            }
            //Comprobar que ambas pilas estan vacías
            if(stack.empty()){
                if(isCorrectString){
                    System.out.println("true");
                }else{
                    System.out.println("false");
                }
            }else{
                System.out.println("false");
                stack.removeAllElements();
            }
        }
    }
}