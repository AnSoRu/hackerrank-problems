package interviewpreparation.arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class NewYearCaos {

    private static final Scanner scanner = new Scanner(System.in);

    //Como maximo una persona puede hacer 2 bribes
    //Los valores indican la posicion inicial que tenian en el array de 1 a n

    private static Integer [] swapPositions(int [] arr, int indexpositionA, int indexpositionB){
        int [] arrCopySwapped = Arrays.copyOf(arr,arr.length);
        int tempA = arrCopySwapped[indexpositionA];
        arrCopySwapped[indexpositionA] = arrCopySwapped[indexpositionB];
        arrCopySwapped[indexpositionB] = tempA;
        Integer [] integersArray = new Integer[arrCopySwapped.length];
        for(int i = 0; i < integersArray.length; i++){
            integersArray[i] = arrCopySwapped[i];
        }
        return integersArray;
    }

    public static boolean isSorted(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] > a[i + 1]) {
                return false;
            }
        }
        return true;
    }

    private static void minimumBribes(int[] q, int steps) {
        //Esta ordenado??
        if(isSorted(q)){
            System.out.println(steps);
        }else{
            int i = 0;
            while(i < q.length-1){
                if(q[i] > q[i+1]){
                    //Se ha desplazado el q[i]
                    Integer [] swapped = swapPositions(q,i,i+1);
                    int [] swappedAux = new int[swapped.length];
                    for(int j = 0 ; j < swapped.length; j++){
                        swappedAux[j] = swapped[j];
                    }
                    minimumBribes(swappedAux,steps+1);
                }else if(i < q.length - 2){
                    if(q[i] > q[i+2]){
                        Integer [] swapped = swapPositions(q,i,i+1);
                        int [] swappedAux = new int[swapped.length];
                        for(int j = 0 ; j < swapped.length; j++){
                            swappedAux[j] = swapped[j];
                        }
                        minimumBribes(swappedAux,steps+1);
                    }
                }
                i++;
            }
        }
    }

    public static void main(String[] args) {
        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] q = new int[n];

            String[] qItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int qItem = Integer.parseInt(qItems[i]);
                q[i] = qItem;
            }

            minimumBribes(q,0);
        }

        scanner.close();

    }
}
