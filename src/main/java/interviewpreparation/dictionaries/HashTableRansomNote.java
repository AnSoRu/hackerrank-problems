package interviewpreparation.dictionaries;

import java.util.HashMap;
import java.util.Scanner;

public class HashTableRansomNote {

    // Complete the checkMagazine function below.
    static void checkMagazine(String[] magazine, String[] note) {
        //Imprimir Yes si la nota puede construirse con las palabras de la magazine
        //Tener en cuenta que hay palabras que pueden aparecer varias veces
        HashMap<String,Integer> magazineWords = new HashMap<>();
        for(String word: magazine){
            if(!magazineWords.containsKey(word)){
                magazineWords.put(word,1);
            }else{
                int contAux = magazineWords.get(word);
                contAux++;
                magazineWords.put(word,contAux);
            }
        }
        int i = 0;
        boolean notPossible = false;
        while(i < note.length){
            if(magazineWords.containsKey(note[i])){
                int cont = magazineWords.get(note[i]);
                if(cont > 0){
                    cont = cont - 1;
                    magazineWords.put(note[i],cont);
                }else{
                    notPossible = true;
                    break;
                }
            }else{
                notPossible = true;
                break;
            }
            i++;
        }
        if(notPossible){
            System.out.println("No");
        }else{
            System.out.println("Yes");
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] mn = scanner.nextLine().split(" ");

        int m = Integer.parseInt(mn[0]);

        int n = Integer.parseInt(mn[1]);

        String[] magazine = new String[m];

        String[] magazineItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < m; i++) {
            String magazineItem = magazineItems[i];
            magazine[i] = magazineItem;
        }

        String[] note = new String[n];

        String[] noteItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            String noteItem = noteItems[i];
            note[i] = noteItem;
        }

        checkMagazine(magazine, note);

        scanner.close();
    }
}
