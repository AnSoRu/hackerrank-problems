package interviewpreparation.dictionaries;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class TwoStrings {

    //Dadas 2 cadenas determinar si comparten alguna subcadena
    //Una substring puede ser de un solo caracter

    // Complete the twoStrings function below.
    /*static String twoStrings(String s1, String s2) {
        char [] s1chars = s1.toCharArray();
        String aux = "";
        int longsubstring = 1;
        int indexInicial = 0;
        while (longsubstring <= s1.length()){
            while(indexInicial+longsubstring <= s1.length()){
                String stringAux = s1.substring(indexInicial,indexInicial+longsubstring);
                if(s2.contains(stringAux)){
                    return "YES";
                }
                indexInicial++;
            }
            indexInicial = 0;
            longsubstring++;
        }
        return "NO";
    }*/

    static String twoStrings(String s1, String s2){
        Set<String> setS1 = new HashSet<>();
        char [] s1Chars = s1.toCharArray();
        for(int i = 0; i < s1Chars.length; i++){
            setS1.add(String.valueOf(s1Chars[i]));
        }
        char [] s2Chars = s2.toCharArray();
        for(int i = 0; i < s2Chars.length; i++){
            if(setS1.contains(s2Chars[i])){
                return "YES";
            }
        }
        return "NO";
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\IntelliJProjects\\HackerrankProblems\\src\\main\\resources\\salida.txt"));

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String s1 = scanner.nextLine();

            String s2 = scanner.nextLine();

            String result = twoStrings(s1, s2);

            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
