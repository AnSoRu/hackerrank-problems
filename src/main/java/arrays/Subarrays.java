package arrays;

import java.util.Scanner;

public class Subarrays {

    //Un subarray de un array de 'n' elementos (son elementos seguidos)
    /*
     Ej: [1,2,3] -> todos los sub-arrays son [1],[2],[3],[1,2],[2,3] y [1,2,3]
     La suma de un array es la suma de todos sus componentes
     Una suma es positiva si el total es positivo
     Una suma es negativa si el total es negativo
     Imprimir el numero de sub_arrays que tengan suma negativa
     */
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt(); //longitud del array
        int [] arr = new int[n];
        for(int i = 0; i < n; i++){
            arr[i] = scan.nextInt();
        }
        int numberOfSubarrays = 0;
        //maximo sub-array = n
        int longitudSubarray = 1; //Numero de elementos que tengo que coger
        while(longitudSubarray < arr.length){
            for(int j = 0 ; j < arr.length; j++){
                int sumaSubArray = 0;
                if(j+longitudSubarray <= arr.length){
                    for(int i = j; i < (j+longitudSubarray); i++){
                        sumaSubArray = sumaSubArray + arr[i];
                    }
                    if(sumaSubArray < 0){
                        numberOfSubarrays++;
                    }
                }
            }
            longitudSubarray++;
        }
        //Falta comprobar si la suma de todo el array es negativa
        int sumaSubArray = 0;
        for(int i = 0; i < arr.length; i++){
            sumaSubArray = sumaSubArray + arr[i];
        }
        if(sumaSubArray < 0){
            numberOfSubarrays++;
        }
        System.out.println(numberOfSubarrays);
    }
}
