package binarynumbers;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    /*
     Dado un numero en base 10
     Convertirlo a binario
     Obtener el numero máximo de 1's consecutivos de ese binario
     */

   /* private static int maximumConsecutiveOnes(String binaryNumber){
       int max = 1;
        if(Integer.valueOf(binaryNumber)==0){
            return 0;
        }
        char [] binaryNumberCharArray = binaryNumber.toCharArray();
        int contAux = 0;
        for(int i = 0; i < binaryNumberCharArray.length ;i++){
            if(binaryNumberCharArray[i]=='1'){
                contAux++;
            }else{
                if(contAux >= max){
                    max = contAux;
                }
                contAux = 0;
            }
        }
        if(max>=contAux){
            return max;
        }else if(max < contAux){
            return contAux;
        }
        return 1;
    }*/

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        scanner.close();
        int rem=0,s=0,t=0;
        while(n>0){
            rem=n%2;
            n=n/2;
            if(rem==1){
                s++;
                if(s>=t){
                    t=s;
                }
            }
            else{
                s=0;
            }
        }
        System.out.println(t);
    }

}
