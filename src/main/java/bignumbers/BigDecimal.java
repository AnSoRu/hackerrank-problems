package bignumbers;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class BigDecimal {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        //Input
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] s = new String[n + 2];
        for (int i = 0; i < n; i++) {
            s[i] = sc.next();
        }
        sc.close();

        Arrays.sort(s, 0, n, Collections.reverseOrder(new Comparator() {
            public int compare(Object o1, Object o2) {
                java.math.BigDecimal a = new java.math.BigDecimal((String)o1);
                java.math.BigDecimal b = new java.math.BigDecimal((String)o2);
                return a.compareTo(b);
            }
        }));

        for (int i = 0; i < n; i++) {
            System.out.println(s[i]);
        }

    }
}
