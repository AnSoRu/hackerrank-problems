package bignumbers;

import java.util.Scanner;

public class BigInteger {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        java.math.BigInteger bigIntegerA = scanner.nextBigInteger();
        java.math.BigInteger bigIntegerB = scanner.nextBigInteger();

        System.out.println(bigIntegerA.add(bigIntegerB));
        System.out.println(bigIntegerA.multiply(bigIntegerB));
    }
}
