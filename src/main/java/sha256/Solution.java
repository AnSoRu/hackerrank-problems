package sha256;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        MessageDigest sha256;
        try{
            sha256 = MessageDigest.getInstance("SHA-256");
            sha256.update(inputString.getBytes());
            byte [] digest = sha256.digest();
            String myHash = DatatypeConverter.printHexBinary(digest).toLowerCase();
            System.out.println(myHash);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            scanner.close();
        }
    }
}
