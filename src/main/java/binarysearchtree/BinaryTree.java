package binarysearchtree;

import org.junit.Test;

import java.util.LinkedList;
import java.util.Queue;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

class Node{
    int value;
    Node left;
    Node right;

    Node(int value) {
        this.value = value;
        right = null;
        left = null;
    }
}

public class BinaryTree {

    private Node root;

    private Node addRecursive(Node current, int value){
        if(current == null){
            return new Node(value);
        }
        if(value < current.value){
            current.left = addRecursive(current.left,value);
        }else if(value > current.value){
            current.right = addRecursive(current.right,value);
        }else{
            return current;
        }
        return current;
    }

    public void add(int value){
        root = addRecursive(root,value);
    }

    private BinaryTree createBinaryTree(){
        BinaryTree bt = new BinaryTree();

        bt.add(6);
        bt.add(4);
        bt.add(8);
        bt.add(3);
        bt.add(5);
        bt.add(7);
        bt.add(9);

        return bt;
    }

    private boolean containsNodeRecursive(Node current, int value){
        if(current == null){
            return false;
        }
        if(value == current.value){
            return true;
        }
        return value < current.value ? containsNodeRecursive(current.left,value): containsNodeRecursive(current.right,value);
    }

    public boolean containsNode(int value){
        return containsNodeRecursive(root,value);
    }

    private int findSmallestValue(Node root){
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    //DELETING ELEMENT
    private Node deleteRecursive(Node current, int value){
        if(current == null){
            return null;
        }
        if(value == current.value){
            //Aqui pueden ocurrir 3 casos
            //Que el nodo a borrar:
            //1) No tenga hijos
            if(current.left == null && current.right == null){
                return null;
            }
            //2) Tenga solo un hijo
            if(current.right == null){
                return current.left;
            }
            if(current.left == null){
                return current.right;
            }
            //3) Tenga 2 hijos
            //3.1) Primero encontrar el nodo que reemplazará al eliminado. Se usará el nodo más pequeño a ser borrado del sub-arbol derecho
            int smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursive(current.right,smallestValue);
            return current;
        }
        if(value < current.value){
            current.left = deleteRecursive(current.left,value);
            return current;
        }
        current.right = deleteRecursive(current.right,value);
        return current;
    }

    public void delete(int value){
        root = deleteRecursive(root,value);
    }

    //TRAVERSING THE TREE
    //DEPTH-FIRST
    //Primero se va lo más profundo que se pueda en cada hijo antes de explorar a su nodo sibling
    //Tres maneras de recorrerlo
    //in-order
    //pre-order
    //post-order

    //Visita inOrder
    //Primero se visita el sub-arbol izquierdo, luego la raiz y luego el sub-arbol derecho
    public void traverseInOrder(Node node){
        if(node != null){
            traverseInOrder(node.left);
            System.out.print(" " + node.value);
            traverseInOrder(node.right);
        }
    }

    //Visita preOrder
    //Primero se visita la raiz, luego el subarbol izquierdo y luego el derecho
    public void traversePreOrder(Node node){
        if(node != null){
            System.out.print(" " + node.value);
            traversePreOrder(node.left);
            traversePreOrder(node.right);
        }
    }

    //BREADTH-FIRST
    //Visitar todos los nodos de un nivel antes de avanzar al siguiente
    //Tambien se le conoce como level-order, visita todos los niveles empezando por la raiz y de izquierda a derecha
    //En esta implementacion se utilizará una cola para ir guardando los nodos de cada nivel en orden
    public void traverseLevelOrder(){
        if(root == null){
            return;
        }
        Queue<Node> nodes = new LinkedList<>();
        nodes.add(root);

        while (!nodes.isEmpty()){
            Node node = nodes.remove();
            System.out.print(" " + node.value);

            if(node.left != null){
                nodes.add(node.left);
            }
            if(node.right != null){
                nodes.add(node.right);
            }
        }
    }

    /*
    @Test
    public void givenABinaryTree_WhenAddingElements_ThenTreeContainsThoseElements() {
        BinaryTree bt = createBinaryTree();

        assertTrue(bt.containsNode(6));
        assertTrue(bt.containsNode(4));

        assertFalse(bt.containsNode(1));
    }

    @Test
    public void givenABinaryTree_WhenDeletingElements_ThenTreeDoesNotContainThoseElements() {
        BinaryTree bt = createBinaryTree();

        assertTrue(bt.containsNode(9));
        bt.delete(9);
        assertFalse(bt.containsNode(9));
    }*/

    public static void main(String [] args){
        BinaryTree bt = new BinaryTree();
        BinaryTree btCreate = bt.createBinaryTree();
        System.out.println("InOrder");
        btCreate.traverseInOrder(btCreate.root); //Salida -> 3 4 5 6 7 8 9
        System.out.println(" ");
        System.out.println("PreOrder");
        btCreate.traversePreOrder(btCreate.root);
        System.out.println(" ");
        System.out.println("LevelOrder");
        btCreate.traverseLevelOrder();
    }
}
