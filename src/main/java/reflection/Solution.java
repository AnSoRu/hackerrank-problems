package reflection;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;

class Student {
    private String name;
    private String id;
    private String email;

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void anotherMethod(){}
    public void anotherMethod1(){}
}

public class Solution {

    /*
     Reflection es la capacidad de Java de consultar los atributos de una clase en TIEMPO DE EJECUCIÓN
     //EJ: se pueden recuperar los atributos publicos de una clase con el metodo getDeclaredMethods();
     */
    public static void main(String[] args){
        //Class student = Student.class;
        Method[] methods = Student.class.getDeclaredMethods(); //Este solo muestra los de la clase Student

        ArrayList<String> methodList = new ArrayList<>();
        for(Method method: methods){
            methodList.add(method.getName());
        }
        Collections.sort(methodList);
        for(String name: methodList){
            System.out.println(name);
        }
    }
}
