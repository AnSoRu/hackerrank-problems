package md5;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        MessageDigest md5;
        try{
            md5 = MessageDigest.getInstance("MD5");
            md5.update(inputString.getBytes());
            byte [] digest = md5.digest();
            String myHash = DatatypeConverter.printHexBinary(digest).toLowerCase();
            System.out.println(myHash);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            scanner.close();
        }
    }
}
