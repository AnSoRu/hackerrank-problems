package strings;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagContentExtractor {

    //Esta solución es correcta pero lenta para el corrector del Hackerrank

    private static String extractContent(String taggedText){
        String result;
        StringBuilder sb = new StringBuilder();
        if(taggedText.length()>0){
            Pattern pattern = Pattern.compile("<(.+)>(<(.+)>)*([^<]+)(</\\3>)*</\\1>");
            Matcher m = pattern.matcher(taggedText);
            result = "";
            boolean matchFound = false;
            while (m.find()) {
                System.out.println(m.group());
                if(sb.length()>0){
                    sb.append(result);
                    sb.append("\n");
                }
                sb.append(m.group(4));
                matchFound = true;
            }
            if ( ! matchFound) {
                sb.append("None");
            }
        }
        return sb.toString();
    }

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());
        while(testCases>0){
            String line = in.nextLine();
            System.out.println(TagContentExtractor.extractContent(line));
            testCases--;
        }
    }
}
