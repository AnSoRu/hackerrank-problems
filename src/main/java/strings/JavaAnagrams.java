package strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class JavaAnagrams {

    static boolean isAnagram(String a, String b) {
        // Complete the function
        //Anagramas son dos palabras que tienen la misma frecuencia de letras sin diferenciar mayusculas y minusculas
        Map<Character,Integer> contadorA = new HashMap<Character,Integer>();
        Map<Character,Integer> contadorB = new HashMap<Character,Integer>();
        char[] aMinusculasArray = a.toLowerCase().toCharArray();
        char[] bMinusculasArray = b.toLowerCase().toCharArray();
        for(int i = 0 ; i < aMinusculasArray.length; i++){
            char aux = aMinusculasArray[i];
            if(!contadorA.containsKey(aux)){
                contadorA.put(aux,1);
            }else{
                int contAux = contadorA.get(aux);
                contAux++;
                contadorA.put(aux,contAux);
            }
        }
        for(int i = 0; i < bMinusculasArray.length; i++){
            char aux = bMinusculasArray[i];
            if(!contadorB.containsKey(aux)){
                contadorB.put(aux,1);
            }else{
                int contAux = contadorB.get(aux);
                contAux++;
                contadorB.put(aux,contAux);
            }
        }
        Set<Character> keySetContadorA = contadorA.keySet();
        Set<Character> keySetContadorB = contadorB.keySet();
        Set<Character> keySet;
        if(keySetContadorA.size() >= keySetContadorB.size()){
            keySet = keySetContadorA;
        }else{
            keySet = keySetContadorB;
        }
        for(Character c: keySet){
            if(!contadorB.containsKey(c)){
                return false;
            }else{
                if(!contadorB.get(c).equals(contadorA.get(c))){
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
    }
}
