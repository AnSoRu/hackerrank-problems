package strings;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String A=sc.next();
        String B=sc.next();
        /* Enter your code here. Print output to STDOUT. */
        //Ambas estan en lowercase
        System.out.println(A.length() + B.length());

        System.out.println(A.compareTo(B) > 0 ? "Yes":"No");

        String firstLetterA = A.substring(0,1).toUpperCase();
        String firstLetterB = B.substring(0,1).toUpperCase();
        String restoA = A.substring(1,A.length());
        String restoB = B.substring(1,B.length());

        String finalA = firstLetterA + restoA;
        String finalB = firstLetterB + restoB;

        System.out.println(finalA + " " + finalB);

    }
}



