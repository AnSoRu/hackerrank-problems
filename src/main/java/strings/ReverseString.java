package strings;

import java.util.Scanner;

public class ReverseString {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String A=sc.next();
        /* Enter your code here. Print output to STDOUT. */
        StringBuilder sbReversed = new StringBuilder();
        sbReversed.append(A);
        String reversed = sbReversed.reverse().toString();
        if(A.equals(reversed)){
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }
    }
}
