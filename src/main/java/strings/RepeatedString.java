package strings;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class RepeatedString {

    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        /*StringBuilder sAuxBuilder = new StringBuilder().append(s);
        while(sAuxBuilder.length()<n){
            sAuxBuilder.append(s);
        }
        char [] stringCharArray = sAuxBuilder.toString().toCharArray();
        long result = 0L;
        for(int i = 0; i < n; i++){
            if(stringCharArray[i]=='a'){
                result++;
            }
        }
        return result;*/
        long q = n/s.length();
        long r = n%s.length();
        if(!s.contains("a")) return 0L;

        return s.length()>n? aux(s,r): q*aux(s,s.length()) + aux(s,r);

    }

    private static int aux(String s,long e){
        int a = 0;
        for(int i = 0; i < e; i++){
            if(s.charAt(i) == 'a') a++;
        }
        return a;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\IntelliJProjects\\HackerrankProblems\\src\\main\\resources\\salida.txt"));

        String s = scanner.nextLine();

        long n = scanner.nextLong();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        long result = repeatedString(s, n);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
