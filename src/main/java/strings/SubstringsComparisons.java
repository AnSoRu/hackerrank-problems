package strings;

import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class SubstringsComparisons {

    //Lexicografico: (alfabético) A < B < C ... < a < b < c ...< z

    /**
     * Dados una cadena 's' y un entero 'k' encontrar la menor y mayor subcadena lexicográfica de longitud k
     * EJ:
     * intput:
     * welcometojava
     * 3
     * output:
     * ava
     * wel
     */
    /*public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";

        char [] charString = s.toCharArray();
        char small = charString[0];
        char big = charString[0];
        int indexSmallest = 0;
        int indexBiggest = 0;
        for(int i = 0; i < charString.length ; i++){
            if(charString[i] < small){
                small = charString[i];
                indexSmallest = i;
            }else{
                if(big < charString[i]){
                    big = charString[i];
                    indexBiggest = i;
                }
            }
        }
        if((indexSmallest + k - 1) < charString.length){
            smallest = s.substring(indexSmallest,indexSmallest+k);
        }
        if((indexBiggest + k - 1) < charString.length){
            largest = s.substring(indexBiggest,indexBiggest+k);
        }
        return smallest + "\n" + largest;
    }*/

    // SortedSet
    public static String getSmallestAndLargest(String s, int k) {
        SortedSet<String> sets=new TreeSet<String>();
        for(int i=0;i<=s.length()-k;i++){
            sets.add(s.substring(i,i+k));
        }
        return sets.first() + "\n" + sets.last();
    }


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();

        System.out.println(getSmallestAndLargest(s, k));
    }
}
