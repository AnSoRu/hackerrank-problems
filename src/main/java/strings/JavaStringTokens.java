package strings;

import java.util.Scanner;

public class JavaStringTokens {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine().trim();
        // Write your code here.
        scan.close();
        if(s.length() > 0){
            String [] tokens = s.split("[!,?._'@\\s]+");
            if(tokens!=null && tokens.length > 0){
                System.out.println(tokens.length);
                for(String sAux: tokens){
                    System.out.println(sAux);
                }
            }else{
                System.out.println("0");
            }
        }else{
            System.out.println("0");
        }
    }
}
