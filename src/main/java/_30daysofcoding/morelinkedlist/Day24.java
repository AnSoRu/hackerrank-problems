package _30daysofcoding.morelinkedlist;

import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeSet;

class Node{
    int data;
    Node next;
    Node(int d){
        data=d;
        next=null;
    }

}

public class Day24 {

    /*
     More Linked List

     */
    public static Node removeDuplicates(Node head) {

        HashSet<Integer> hashSet = new HashSet<>();
        Node headCopy;
        if(head!=null){
            headCopy = head;
        }else{
            return head;
        }
        while(headCopy.next!=null){
            hashSet.add(headCopy.data);
            headCopy = headCopy.next;
        }
        //Add the last one
        hashSet.add(headCopy.data);
        headCopy = null;
        Node previousHead = null;
        //Sort the HashSet in ascending order
        TreeSet<Integer> ordered = new TreeSet<>(hashSet);
        for(Integer i: ordered){
            head.data = i;
            if(headCopy==null){
                headCopy = head;
            }
            head.next = new Node(Integer.MIN_VALUE);
            previousHead = head;
            head = head.next;
        }
        previousHead.next = null;
        head = headCopy;
        return head;
    }

    public static  Node insert(Node head,int data)
    {
        Node p=new Node(data);
        if(head==null)
            head=p;
        else if(head.next==null)
            head.next=p;
        else
        {
            Node start=head;
            while(start.next!=null)
                start=start.next;
            start.next=p;

        }
        return head;
    }
    public static void display(Node head)
    {
        Node start=head;
        while(start!=null)
        {
            System.out.print(start.data+" ");
            start=start.next;
        }
    }
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        Node head=null;
        int T=sc.nextInt();
        while(T-->0){
            int ele=sc.nextInt();
            head=insert(head,ele);
        }
        head=removeDuplicates(head);
        display(head);
    }
}
