package _30daysofcoding;


import java.math.BigInteger;
import java.util.Scanner;

public class Day25 {


    /*
      Running time and complexity
      Si es posible usar un algoritmo con complejidad sqrt(n)
     */
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        int n = Integer.valueOf(scanner.nextLine());
        int [] numbers = new int[n];
        for(int i = 0; i < n; i++){
            numbers[i] = scanner.nextInt();
        }
        BigInteger bi;
        for(int i = 0; i < numbers.length; i++){
            bi = new BigInteger(String.valueOf(numbers[i]));
            if(bi.isProbablePrime(3)){
                System.out.println("Prime");
            }else{
                System.out.println("Not prime");
            }
        }

    }
}
