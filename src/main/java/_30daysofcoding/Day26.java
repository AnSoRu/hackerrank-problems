package _30daysofcoding;

import java.time.LocalDate;
import java.util.Scanner;

public class Day26 {

    //Nested Logic
    /*
    Objective
Today's challenge puts your understanding of nested conditional statements to the test. You already have the knowledge to complete this challenge, but check out the Tutorial tab for a video on testing!

Task
Your local library needs your help! Given the expected and actual return dates for a library book, create a program that calculates the fine (if any). The fee structure is as follows:

    If the book is returned on or before the expected return date, no fine will be charged (i.e.:

.
If the book is returned after the expected return day but still within the same calendar month and year as the expected return date,
.
If the book is returned after the expected return month but still within the same calendar year as the expected return date, the
.
If the book is returned after the calendar year in which it was expected, there is a fixed fine of

    .

Input Format

The first line contains
space-separated integers denoting the respective , , and on which the book was actually returned.
The second line contains space-separated integers denoting the respective , , and

on which the book was expected to be returned (due date).

Constraints

Output Format

Print a single integer denoting the library fine for the book received as input.

Sample Input

9 6 2015
6 6 2015

Sample Output

45

Explanation

Given the following return dates:
Actual:

Expected: Because , we know it is less than a year late.
Because , we know it's less than a month late.
Because

, we know that it was returned late (but still within the same month and year).

Per the library's fee structure, we know that our fine will be
. We then print the result of as our output.
     */

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        String date1String = scanner.nextLine();
        String [] date1StringSplit = date1String.split(" ");
        String date2String = scanner.nextLine();
        String [] date2StringSplit = date2String.split(" ");

        int day1 = Integer.valueOf(date1StringSplit[0]);
        int month1 = Integer.valueOf(date1StringSplit[1]);
        int year1 = Integer.valueOf(date1StringSplit[2]);

        int day2 = Integer.valueOf(date2StringSplit[0]);
        int month2 = Integer.valueOf(date2StringSplit[1]);
        int year2 = Integer.valueOf(date2StringSplit[2]);

        LocalDate localDate1 = LocalDate.of(year1,month1,day1); //Fecha en la que fue devuelto
        LocalDate localDate2 = LocalDate.of(year2,month2,day2); //Fecha en la que se tenía que haber devuelto

        int differenceDays = localDate1.until(localDate2).getDays();
        int differenceMonths = localDate1.until(localDate2).getMonths();
        int differenceYears = localDate1.until(localDate2).getYears();

        int totalFine = 0;

        if(year1 - year2 > 0 ){
            totalFine = 10000;
        }else{
            if(differenceYears < 0){
                totalFine = totalFine + 1000;
            }else if(differenceMonths < 0){
                totalFine = totalFine + (500 * Math.abs(differenceMonths));
            }else if(differenceDays < 0){
                totalFine = totalFine + (15 * Math.abs(differenceDays));
            }
        }

        System.out.println(totalFine);
    }
}
