package _30daysofcoding;

import java.util.Scanner;

public class Day6 {

    /*
    Objective
Today we're expanding our knowledge of Strings and combining it with what we've already learned about loops. Check out the Tutorial tab for learning materials and an instructional video!

Task
Given a string,
, of length that is indexed from to , print its even-indexed and odd-indexed characters as

space-separated strings on a single line (see the Sample below for more detail).

Note:

is considered to be an even index.

Input Format

The first line contains an integer,
(the number of test cases).
Each line of the subsequent lines contain a String,

.

Constraints

Output Format

For each String
(where ), print 's even-indexed characters, followed by a space, followed by

's odd-indexed characters.

Sample Input

2
Hacker
Rank

Sample Output

Hce akr
Rn ak

Explanation

Test Case 0:







The even indices are , , and , and the odd indices are , , and . We then print a single line of space-separated strings; the first string contains the ordered characters from 's even indices (), and the second string contains the ordered characters from 's odd indices (

).

Test Case 1:





The even indices are and , and the odd indices are and . We then print a single line of space-separated strings; the first string contains the ordered characters from 's even indices (), and the second string contains the ordered characters from 's odd indices ().
     */
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int testCases = scan.nextInt();
        for(int i = 0; i < testCases; i++){
            char[] inputString = scan.next().toCharArray();
            StringBuilder oddString = new StringBuilder();
            StringBuilder evenString = new StringBuilder();

            for(int j = 0; j < inputString.length; j++) {
                if( (j & 1) == 0) {
                    evenString.append(inputString[j]);
                }
                else {
                    oddString.append(inputString[j]);
                }
            }
            System.out.println(evenString + " " + oddString);
        }
        scan.close();
    }
}
