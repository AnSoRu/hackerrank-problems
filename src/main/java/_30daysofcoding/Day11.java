package _30daysofcoding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Day11 {

    /*
    Objective
Today, we're building on our knowledge of Arrays by adding another dimension. Check out the Tutorial tab for learning materials and an instructional video!

Context
Given a
2D Array,

:

1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0

We define an hourglass in
to be a subset of values with indices falling in this pattern in

's graphical representation:

a b c
  d
e f g

There are
hourglasses in

, and an hourglass sum is the sum of an hourglass' values.

Task
Calculate the hourglass sum for every hourglass in

, then print the maximum hourglass sum.

Input Format

There are
lines of input, where each line contains space-separated integers describing 2D Array ; every value in will be in the inclusive range of to

.

Constraints

Output Format

Print the largest (maximum) hourglass sum found in

.

Sample Input

1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 2 4 4 0
0 0 0 2 0 0
0 0 1 2 4 0

Sample Output

19

Explanation

contains the following hourglasses:

1 1 1   1 1 0   1 0 0   0 0 0
  1       0       0       0
1 1 1   1 1 0   1 0 0   0 0 0

0 1 0   1 0 0   0 0 0   0 0 0
  1       1       0       0
0 0 2   0 2 4   2 4 4   4 4 0

1 1 1   1 1 0   1 0 0   0 0 0
  0       2       4       4
0 0 0   0 0 2   0 2 0   2 0 0

0 0 2   0 2 4   2 4 4   4 4 0
  0       0       2       0
0 0 1   0 1 2   1 2 4   2 4 0

The hourglass with the maximum sum (

) is:

2 4 4
  2
1 2 4

     */
    private static final Scanner scanner = new Scanner(System.in);

    private static int [] getHourglass(int [][] arr, int initialIndexRow, int initialIndexColumn){
        int [] result = new int[7];
        if(arr.length > 0){
            int cont = 0;
            if(initialIndexRow < 4 && initialIndexColumn < 4){
                for(int i = initialIndexRow; i <= initialIndexRow + 2 ; i++){
                    for(int j = initialIndexColumn; j <= initialIndexColumn + 2; j++){
                        if((i==initialIndexRow+1)&&((j==initialIndexColumn)||(j==initialIndexColumn+2))){
                            continue;
                        }else{
                            result[cont] = arr[i][j];
                            cont++;
                        }
                    }
                }
            }else{
                result = null;
            }
        }
        return result;
    }

    private static int getSumOfHourglass(int [] hourglass){
        int result = 0;
        if(hourglass.length > 0){
            for(int i = 0; i < hourglass.length; i++){
                result = result + hourglass[i];
            }
        }else{
            return 0;
        }
        return result;
    }

    public static void main(String[] args) {
        int[][] arr = new int[6][6];

        for (int i = 0; i < 6; i++) {
            String[] arrRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 6; j++) {
                int arrItem = Integer.parseInt(arrRowItems[j]);
                arr[i][j] = arrItem;
            }
        }

        scanner.close();
        List<Integer> sumOfHourglass = new ArrayList<Integer>();
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                int [] partialHourglass = getHourglass(arr,i,j);
                int partialSum = getSumOfHourglass(partialHourglass);
                sumOfHourglass.add(partialSum);
            }
        }
        System.out.println(Collections.max(sumOfHourglass));
    }
}
