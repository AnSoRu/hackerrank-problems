# Hackerrank 30 Days of Coding problems #

This repository contains the solutions for the 30 Days of Code Hackerrank tutorial and<br>
for the Java Language Proficiency Hackerrank problems.

* [30 Days of Code](https://www.hackerrank.com/domains/tutorials/30-days-of-code)
* [Java Language Proficiency](https://www.hackerrank.com/domains/java?badge_type=java)

### Tools ###

* IntelliJ Idea Community 2019.1
* Java 1.8.0_251 64-Bit
